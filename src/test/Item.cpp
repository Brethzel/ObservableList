#include "Item.h"

Item::Item(QObject *parent) : QObject(parent) {}

int Item::frequency() const {
    return m_frequency;
}

QString Item::name() const {
    return m_name;
}

void Item::setFrequency(int frequency) {
    if (m_frequency == frequency)
        return;

    m_frequency = frequency;
    emit frequencyChanged(frequency);
}

void Item::setName(QString name) {
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(name);
}
