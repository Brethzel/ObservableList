#ifndef TEST_H
#define TEST_H

#include <QObject>
#include "Item.h"
#include "ObservableList.h"
#include <QTimer>

class Test : public QObject {
    Q_OBJECT

    Q_PROPERTY(QAbstractListModel* list READ list CONSTANT)

public:
    explicit Test(QObject* parent = nullptr);

    ObservableList<Item>* list() const;

private:
    ObservableList<Item>* m_list;
    QTimer m_timer;

signals:
    void itemChanged(Item* item);
};

#endif
