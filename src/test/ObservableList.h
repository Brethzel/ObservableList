/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <gilles.fernandez10@gmail.com> wrote this file. As long as you retain this
 * notice you can do whatever you want with this stuff. If we meet some day,
 * and you think this stuff is worth it, you can buy me a (belgian) beer in
 * return.
 * Gilles Fernandez
 * ----------------------------------------------------------------------------
 */

#ifndef OBSERVABLELIST_H
#define OBSERVABLELIST_H

#include <QObject>
#include <QAbstractListModel>
#include <QMetaProperty>
#include <QSharedPointer>

#include <wobjectdefs.h>
#include <wobjectimpl.h>

template<typename T> class ObservableList : public QAbstractListModel {
    W_OBJECT(ObservableList)

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override {
        Q_UNUSED(parent)
        return m_list.size();
    }

    QHash<int, QByteArray> roleNames() const override {
        return m_rolesMap;
    }

    virtual QVariant data(const QModelIndex &index, int role) const override {
        return data(index.row(), role);
    }

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override {
        return setData(index.row(), value, role);
    }

public:
    explicit ObservableList(QObject* parent = nullptr) : QAbstractListModel(parent) {
        QMetaObject metaItem(T::staticMetaObject);

        m_rolesMap.insert(Qt::DisplayRole, "modelData");

        const int count = metaItem.propertyCount();
        for (int index = 0; index < count; index++) {
            QMetaProperty metaProp = metaItem.property(index);
            QByteArray propName = QByteArray(metaProp.name());
            m_rolesMap.insert(Qt::UserRole + 1 + index, propName);

            if (metaProp.hasNotifySignal())
                m_signalIndexToRole.insert(metaProp.notifySignalIndex(), Qt::UserRole + 1 + index);
        }

        static const char * HANDLER = "propertyChangedHandler()";
        m_handler = metaObject()->method(metaObject()->indexOfMethod(HANDLER));

        connect(this, &QAbstractListModel::rowsInserted, this, &ObservableList<T>::sizeChanged);
        connect(this, &QAbstractListModel::rowsRemoved, this, &ObservableList<T>::sizeChanged);
    }

    bool isEmpty() const {
        return m_list.isEmpty();
    }

    int size() const {
        return m_list.size();
    }

    const QSharedPointer<T> operator[] (int index) const {
        return at(index);
    }

    const QSharedPointer<T> at(int index) const {
        return m_list[index];
    }

    // How could we register arg type for a templated type? It would allow us using at()
    // in QML instead of QVariant and itemAt().
    QVariant itemAt(int index) const {
        return data(index, Qt::DisplayRole);
    }

    typename QList<T*>::iterator begin() {
        return m_list.begin();
    }

    typename QList<T*>::iterator end() {
        return m_list.end();
    }

    typename QList<T*>::const_iterator constBegin() const {
        return m_list.constBegin();
    }

    typename QList<T*>::const_iterator constEnd() const {
        return m_list.constEnd();
    }

    T* first() const {
        return m_list.first();
    }

    T* last() const {
        return m_list.last();
    }

    const QList<T*>& toList() const {
        return m_list;
    }

    bool contains(T* item) const {
        return contains(sharedPointerFor(item));
    }

    bool contains(QSharedPointer<T> item) const {
        return m_list.contains(item);
    }

    int indexOf(QSharedPointer<T> item) const {
        return indexOf(item.data());
    }

    int indexOf(T* item) const {
        for (int i(0); i < m_list.size(); i++) {
            if (m_list.at(i).data() == item)
                return i;
        }

        return -1;
    }

    QSharedPointer<T> sharedPointerFor(T* pointer) const {
        for (auto item : m_list) {
            if (item.data() == pointer)
                return item;
        }

        return QSharedPointer<T>(pointer);
    }

    virtual QVariant data(int index, const QString& role) const {
        const int roleIndex(m_rolesMap.key(role.toLatin1()));
        return data(index, roleIndex);
    }

    virtual QVariant data(int index, int role = Qt::DisplayRole) const {
        QVariant ret;

        if (m_rolesMap.contains(role) == false)
            return ret;

        if (index < 0 || index > m_list.size())
            return ret;

        QSharedPointer<T> item(m_list.at(index));
        if (item == nullptr)
            return ret;

        if (role == Qt::DisplayRole)
            ret.setValue(item.data());
        else
            ret.setValue(item->metaObject()->property(role - 1 - Qt::UserRole).read(item.data()));

        return ret;
    }

    virtual bool setData(int index, const QVariant &value, const QString& role) {
        const int roleIndex(m_rolesMap.key(role.toLatin1()));
        return setData(index, value, roleIndex);
    }

    virtual bool setData(int index, const QVariant &value, int role) {
        if (m_rolesMap.contains(role) == false)
            return false;

        if (index < 0 || index > m_list.size())
            return false;

        QSharedPointer<T> item(m_list.at(index));
        if (item == nullptr || role == Qt::DisplayRole)
            return false;

        QMetaProperty property(item->metaObject()->property(role - 1 - Qt::UserRole));
        if (property.isWritable() == false)
            return false;

        return property.write(item.data(), value);
    }

    virtual void append(QSharedPointer<T> item) {
        const int index(m_list.size());
        beginInsertRows(QModelIndex(), index, index);
        m_list.append(item);
        listenItem(item);
        endInsertRows();

        emit itemAdded(item.data());
    }

    virtual void prepend(QSharedPointer<T> item) {
        beginInsertRows(QModelIndex(), 0, 0);
        m_list.prepend(item);
        listenItem(item);
        endInsertRows();

        emit itemAdded(item.data());
    }

    virtual void insert(int index, QSharedPointer<T> item) {
        beginInsertRows(QModelIndex(), index, index);
        m_list.insert(index, item);
        listenItem(item);
        endInsertRows();

        emit itemAdded(item.data());
    }

    virtual void move(int from, int to) {
        const int lowest  = qMin(from, to);
        const int highest = qMax(from, to);

        beginMoveRows(QModelIndex(), highest, highest, QModelIndex(), lowest); \
        m_list.move(from, to);
        endMoveRows();
    }

    virtual void replace(QSharedPointer<T> oldItem, QSharedPointer<T> newItem) {
        int index = m_list.indexOf(oldItem);

        if (index < 0 || oldItem == nullptr || newItem == nullptr) \
            return;

        removeOne(oldItem);
        insert(index, newItem);

        itemReplaced(newItem.data(), index);
    }

    virtual void removeOne(QSharedPointer<T> item) {
        if (item == nullptr)
            return;

        int index = m_list.indexOf(item);
        beginRemoveRows(QModelIndex(), index, index);
        m_list.removeOne(item);
        unlistenItem(item);
        endRemoveRows();

        emit itemRemoved(item.data());
    }

    virtual void clear() {
        beginRemoveRows(QModelIndex(), 0, m_list.size() - 1);

        for (auto item : m_list)
            unlistenItem(item);

        m_list.clear();
        endRemoveRows();
    }

    virtual void fillFrom(const ObservableList<T>& sourceList) {
        clear();

        for (int i(0); i < sourceList.size(); i++)
            append(sourceList.at(i));
    }

    W_INVOKABLE(contains, (T*))

    W_INVOKABLE(indexOf, (T*))

    W_INVOKABLE(itemAt, (int))

    W_INVOKABLE(data, (int, const QString&))

public /* signals */:
    void sizeChanged() W_SIGNAL(sizeChanged)
    // How could we register arg type for a templated type? It would allow us sending
    // T* instead of QObject*.
    void itemAdded(QObject* item) W_SIGNAL(itemAdded, item)
    // How could we register arg type for a templated type? It would allow us sending
    // T* instead of QObject*.
    void itemRemoved(QObject* item) W_SIGNAL(itemRemoved, item)
    // How could we register arg type for a templated type? It would allow us sending
    // T* instead of QObject*.
    void itemReplaced(QObject* item, int index) W_SIGNAL(itemReplaced, item, index)

public /* properties */:
    W_PROPERTY(bool, isEmpty READ isEmpty NOTIFY sizeChanged)
    W_PROPERTY(int, size READ size NOTIFY sizeChanged)
    W_PROPERTY(int, length READ size NOTIFY sizeChanged)
    W_PROPERTY(int, size READ size NOTIFY sizeChanged)

protected:
    QList<QSharedPointer<T>> m_list;
    QHash<int, QByteArray> m_rolesMap;
    QMetaMethod m_handler;
    QHash<int, int> m_signalIndexToRole;

    void listenItem(QSharedPointer<T> item) {
        if (item == nullptr)
            return;

        int count(item->metaObject()->propertyCount());
        for (int i(0); i < count; i++) {
            QMetaProperty prop(item->metaObject()->property(i));
            if (strcmp(prop.name(), "objectName") == 0
                || prop.isReadable() == false
                || prop.hasNotifySignal() == false
            ) {
                continue;
            }

            QMetaMethod notifier = prop.notifySignal();

            connect(item.data(), notifier, this, m_handler, Qt::UniqueConnection);
        }
    }

    void unlistenItem(QSharedPointer<T> item) {
        if (item == nullptr)
            return;

        disconnect(this, nullptr, item.data(), nullptr);
        disconnect(item.data(), nullptr, this, nullptr);
    }

protected /*slots*/:
    void propertyChangedHandler() {
        T* item = qobject_cast<T*>(sender());
        int index = indexOf(item);
        int signalIndex = senderSignalIndex();
        QModelIndex rowIndex = QAbstractListModel::index(index, 0, QModelIndex());

        emit dataChanged(rowIndex, rowIndex, { m_signalIndexToRole.value(signalIndex) });
    }

    W_SLOT(propertyChangedHandler, W_Access::Protected)
};

W_OBJECT_IMPL(ObservableList<T>, template<typename T>)

#endif
