#ifndef ITEM_H
#define ITEM_H

#include <QObject>

class Item : public QObject {
    Q_OBJECT

    Q_PROPERTY(int frequency READ frequency WRITE setFrequency NOTIFY frequencyChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    explicit Item(QObject* parent = nullptr);

    int frequency() const;
    QString name() const;

    void setFrequency(int frequency);
    void setName(QString name);

private:
    int m_frequency;
    QString m_name;

signals:
    void frequencyChanged(int frequency);
    void nameChanged(QString name);
};

#endif
