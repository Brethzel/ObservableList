#include "Test.h"
#include <QDebug>

Test::Test(QObject *parent)
:
    QObject(parent),
    m_list(new ObservableList<Item>(this)
) {
    for (int i(0); i < 10; i++) {
        QSharedPointer<Item> item(new Item(m_list));
        item->setName("Item " + QString::number(i));
        item->setFrequency(i * 100);
        m_list->append(item);
    }

    m_timer.setSingleShot(true);
    m_timer.setInterval(3000);

    connect(&m_timer, &QTimer::timeout, [this] () {
        QSharedPointer<Item> item(new Item(m_list));
        item->setName("appended item");
        item->setFrequency(0);
        m_list->append(item);

        QSharedPointer<Item> item2(m_list->at(2));
        item2->setName("changed name");
    });

    m_timer.start();
}

ObservableList<Item>* Test::list() const {
    return m_list;
}
