import QtQuick 2.3
import test.types 1.0

Item {
    id: root

    Test {
        id: test
    }

    Column {
        anchors.centerIn: parent

        Repeater {
            model: test.list

            delegate: Text {
                text: name
//                text: test.list.data(index, "name")
//                text: test.list.itemAt(index).name
//                text: test.list[index]
//                text: test.list.size
//                text: modelData.name
            }
        }

        Text {
            text: test.list.length
        }
    }

    Component.onCompleted: {
        for (var i in test.list) {
            console.log(i + " " + test.list[i])
        }
    }
}
