#include <QGuiApplication>
#include <QSurfaceFormat>
#include <QtQml>

#include <config.h>

#include "internal/AutoRefreshView.h"

#include "test/Test.h"
#include "test/Item.h"
#include "test/ObservableList.h"

int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);

    AutoRefreshView view;
    view.setTitle("ObservableList");
    view.setMinimumSize(QSize(400, 600));
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    QSurfaceFormat format = view.format();
    format.setSamples(16);
    view.setFormat(format);

    qmlRegisterType<Test>("test.types", 1, 0, "Test");
    qmlRegisterType<Item>("test.types", 1, 0, "TestItem");

#ifdef QT_DEBUG
    QString path(OBSERVABLELIST_PROJECT_FOLDER);
    path += "/src/main.qml";
    view.setSource(QUrl(path));
#else
    view.setSource(QUrl("qrc:/src/main.qml"));
#endif

    view.show();

    return app.exec();
}
