include(ExternalProject)

macro(add_external_library NAME URL)
	file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build)

	file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME})

	if (${URL} MATCHES ".*.git")
		ExternalProject_Add(
			${NAME}
			GIT_REPOSITORY ${URL}
			PATCH_COMMAND ""
			INSTALL_COMMAND ""
			UPDATE_COMMAND ""
			SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${NAME}
			BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}
		)
	else()
		ExternalProject_Add(
			${NAME}
			URL ${URL}
			PATCH_COMMAND ""
			INSTALL_COMMAND ""
			UPDATE_COMMAND ""
			SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${NAME}
			BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}
		)
	endif()

	set(${NAME}_INCLUDE_DIRS ${CMAKE_CURRENT_BINARY_DIR}/${NAME}/include)

	set(${NAME}_LIBRARIES ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME}/${CMAKE_SHARED_LIBRARY_PREFIX}${NAME}${CMAKE_SHARED_LIBRARY_SUFFIX})

	set(${NAME}_LIBRARY_PATH ${CMAKE_CURRENT_BINARY_DIR}/external-libraries-build/${NAME})

	list(APPEND EXTERNAL_LIBRARIES ${NAME})
endmacro()

